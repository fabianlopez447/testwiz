
<!DOCTYPE html>
 
<html lang="es">
 
<head>
<title>Titulo de la web</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="css/style.css" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="stylesheet" href="/favicon.ico" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss" />
</head>

<style>
	.buena{
		color:#04B404;
	}
	.mala{
        color:#FF0000;
	}
	.form-group.resp{
		margin-bottom:0px !important;
	}
</style>
 
<body style="">
<div id="app">
<div class="container" style="margin-top: 40vh;" v-if="menuinicial">
	<div class="col-md-12 col-sm-12">
		<div class="card">
		
		  <div class="card-body">
		  	<center>
		    <h2 class="card-title">Si quieres conocer tu nivel de español realiza nuestros test gratuitos.</h2>
		    <br>	
		    <button class="btn btn-primary" v-on:click="abrirtesta1" onclick="inicio();"> Test A1	</button>
			<button class="btn btn-primary" v-on:click="abrirtesta2" onclick="inicio();"> Test A2   </button>
			<button class="btn btn-primary" v-on:click="abrirtestb1" onclick="inicio();"> Test B1   </button>
		   </center>
		  </div>
		</div>	
	</div>
</div>

<div class="container-fluid">
	 <div id="contenedor">
		<div style="display: none" class="reloj" id="Horas">00</div>
		<div class="reloj" id="Minutos">00</div>
		<div class="reloj" id="Segundos">:00</div>
		<div style="display: none" class="reloj" id="Centesimas">:00</div>
		<div class="reloj" >/15:00</div>
		<input style="display: none;" type="button" class="boton" id="inicio" value="Start &#9658;"  onclick="inicio();"  >
		<input style="display: none;" type="button" class="boton" id="parar" value="Stop &#8718;" onclick="parar();" disabled>
		<input style="display: none;" type="button" class="boton" id="continuar" value="Resume &#8634;" onclick="inicio();" disabled>
		<input style="display: none;" type="button" class="boton" id="reinicio" value="Reset &#8635;" onclick="reinicio();" disabled> 
	</div>
 <div v-if="testa1">
	<h1>Test Nivel A1 Principiante.</h1>
	<!-- Pregunta 1 -->
	 <div class="form-group">
		<label class="testpregunta"> <b>1. Tenemos dos lápices _______. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="1.1" value="Incorrecto2" v-model="a1p1r" :disabled="disabled == 1 ? true : false">
			<label for="1.1" >Verde</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="1.2" value="Incorrecto" v-model="a1p1r" :disabled="disabled == 1 ? true : false">
			<label for="1.2">Rojo</label>
		</div>
		<div class="form-group resp" >	
			<input class="" type="radio" id="1.3" value="Correcto" v-model="a1p1r" :disabled="disabled == 1 ? true : false">
			<label for="1.3">Amarillos</label>
			<div v-bind:class="comprobar1" v-if="resultadosa1" > <h4 > Pregunta número 1: {{resultado1}}</h4> </div>
		</div>
	</div>
	<!-- Pregunta 2 -->
	<div class="form-group">
		<label> <b>2. Venimos a clase _______ la mañana.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio" id="2.1" value="Correcto" v-model="a1p2r" :disabled="disabled == 1 ? true : false">
			<label class="" for="2.1" >por</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="2.2" value="Incorrecto2" v-model="a1p2r" :disabled="disabled == 1 ? true : false">
			<label class="" for="2.2">de</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="2.3" value="Incorrecto" v-model="a1p2r" :disabled="disabled == 1 ? true : false">
			<label class="" for="2.3">en</label>
		</div>
		<div v-bind:class="comprobar2"  v-if="resultadosa1"> <h4>Pregunta número 2: {{resultado2}}</h4> </div>
	</div>
	<!-- Pregunta 3 -->
	<div class="form-group">
		<label> <b>3. ¿______ hermanos tienes?</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="3.1" value="Incorrecto" v-model="a1p3r" :disabled="disabled == 1 ? true : false">
			<label class="" for="3.1" >quién </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="3.2" value="Correcto" v-model="a1p3r" :disabled="disabled == 1 ? true : false">
			<label class="" for="3.2">cuántos</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="3.3" value="Incorrecto2" v-model="a1p3r" :disabled="disabled == 1 ? true : false">
			<label class="" for="3.3">dónde</label>
		</div>
		<div v-bind:class="comprobar3"  v-if="resultadosa1"> <h4>Pregunta número 3: {{resultado3}}</h4> </div>
	</div>
	<!-- Pregunta 4 -->
	<div class="form-group">
		<label> <b>4. Teresa es más alta ______ Javier.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="4.1" value="Incorrecto" v-model="a1p4r" :disabled="disabled == 1 ? true : false">
			<label class="" for="4.1">tan </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="4.2" value="Correcto" v-model="a1p4r" :disabled="disabled == 1 ? true : false">
			<label class="" for="4.2">que</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="4.3" value="Incorrecto2" v-model="a1p4r" :disabled="disabled == 1 ? true : false">
			<label class="" for="4.3">como</label>
		</div>
		<div v-bind:class="comprobar4"  v-if="resultadosa1"> <h4>Pregunta número 4: {{resultado4}}</h4> </div>
	</div>
	<!-- Pregunta 5 -->
	<div class="form-group">
		<label> <b>5. ¿Cuántos años tiene tu padre?</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="5.1" value="Incorrecto" v-model="a1p5r" :disabled="disabled == 1 ? true : false">
			<label class="" for="5.1" valu>cuarentaiocho</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="5.2" value="Incorrecto2" v-model="a1p5r" :disabled="disabled == 1 ? true : false">
			<label class="" for="5.2">quarenta y ocho</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="5.3" value="Correcto" v-model="a1p5r" :disabled="disabled == 1 ? true : false">
			<label class="" for="5.3">quarenta y ocho</label>
		</div>
		<div v-bind:class="comprobar5"  v-if="resultadosa1"> <h4>Pregunta número 5: {{resultado5}}</h4> </div>
	</div>
	<!-- Pregunta 6 -->
	<div class="form-group">
		<label> <b>6. ______ treinta años.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="6.1" value="Correcto" v-model="a1p6r" :disabled="disabled == 1 ? true : false">
			<label class="" for="6.1" valu>tengo </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="6.2" value="Incorrecto" v-model="a1p6r" :disabled="disabled == 1 ? true : false">
			<label class="" for="6.2">soy</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="6.3" value="Incorrecto2" v-model="a1p6r" :disabled="disabled == 1 ? true : false">
			<label class="" for="6.3">estoy</label>
		</div>
		<div v-bind:class="comprobar6"  v-if="resultadosa1"> <h4>Pregunta número 6: {{resultado6}}</h4> </div>
	</div>
	<!-- Pregunta 7 -->
	<div class="form-group">
		<label> <b>7. Yo ________ levanto a las siete todos los lunes.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="7.1" value="Incorrecto" v-model="a1p7r" :disabled="disabled == 1 ? true : false">
			<label class="" for="7.1" valu>se</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="7.2" value="Correcto" v-model="a1p7r" :disabled="disabled == 1 ? true : false">
			<label class="" for="7.2">me</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="7.3" value="Incorrecto2" v-model="a1p7r" :disabled="disabled == 1 ? true : false">
			<label class="" for="7.3">estoy</label>
		</div>
		<div v-bind:class="comprobar7"  v-if="resultadosa1"> <h4>Pregunta número 7: {{resultado7}}</h4> </div>
	</div>
	<!-- Pregunta 8 -->
	<div class="form-group">
		<label> <b>8. Matilda _______ de Alemania, pero _______ en España.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="8.1" value="Incorrecto" v-model="a1p8r" :disabled="disabled == 1 ? true : false">
			<label class="" for="8.1" valu>es /es</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="8.2" value="Incorrecto2" v-model="a1p8r" :disabled="disabled == 1 ? true : false">
			<label class="" for="8.2">está /es</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="8.3" value="Correcto" v-model="a1p8r" :disabled="disabled == 1 ? true : false">
			<label class="" for="8.3">es/está</label>
		</div>
		<div v-bind:class="comprobar8"  v-if="resultadosa1"> <h4>Pregunta número 8: {{resultado8}}</h4> </div>
	</div>
	<!-- Pregunta  9-->
	<div class="form-group">
		<label> <b>9. Mi primo ayer _______ mucho.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="9.1" value="Correcto" v-model="a1p9r" :disabled="disabled == 1 ? true : false">
			<label class="" for="9.1" valu>Estudió</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="9.2" value="Incorrecto" v-model="a1p9r" :disabled="disabled == 1 ? true : false">
			<label class="" for="9.2">Estudia </label>		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="9.3" value="Incorrecto2" v-model="a1p9r" :disabled="disabled == 1 ? true : false">
			<label class="" for="9.3">Ha estudiado </label>
		</div>
		<div v-bind:class="comprobar9"  v-if="resultadosa1"> <h4>Pregunta número 9: {{resultado9}}</h4> </div>	
	</div>
	<!-- Pregunta  10-->
	<div class="form-group">
		<label> <b>10. ¿Y tú de dónde _____ ?</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="10.1" value="Correcto" v-model="a1p10r" :disabled="disabled == 1 ? true : false">
			<label class="" for="10.1" valu>eres</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="10.2" value="Incorrecto" v-model="a1p10r" :disabled="disabled == 1 ? true : false">
			<label class="" for="10.2">es</label>	
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="10.3" value="Incorrecto2" v-model="a1p10r" :disabled="disabled == 1 ? true : false">
			<label class="" for="10.3">son </label>
		</div>
		<div v-bind:class="comprobar10"  v-if="resultadosa1"> <h4>Pregunta número 10: {{resultado10}}</h4> </div>
	</div>
	<!-- Pregunta  11-->
	<div class="form-group">
		<label> <b>11. A nosotros ____ gusta mucho la paella.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="11.1" value="Incorrecto" v-model="a1p11r" :disabled="disabled == 1 ? true : false">
			<label class="" for="11.1" valu>os</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="11.2" value="Correcto" v-model="a1p11r" :disabled="disabled == 1 ? true : false">
			<label class="" for="11.2">nos</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="11.3" value="Incorrecto2" v-model="a1p11r" :disabled="disabled == 1 ? true : false">
			<label class="" for="11.3">les</label>
		</div>
		<div v-bind:class="comprobar11"  v-if="resultadosa1"> <h4>Pregunta número 11: {{resultado11}}</h4> </div>	
	</div>
	<!-- Pregunta  12-->
	<div class="form-group">
		<label> <b>12. Estos niños tienen _____ manos muy sucias.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="12.1" value="Correcto" v-model="a1p12r" :disabled="disabled == 1 ? true : false">
			<label class="" for="12.1" valu>las</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="12.2" value="Incorrecto" v-model="a1p12r" :disabled="disabled == 1 ? true : false">
			<label class="" for="12.2">les</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="12.3" value="Incorrecto2" v-model="a1p12r" :disabled="disabled == 1 ? true : false">
			<label class="" for="12.3">los</label>
		</div>
		<div v-bind:class="comprobar12"  v-if="resultadosa1"> <h4>Pregunta número 12: {{resultado12}}</h4> </div>	
	</div>
	<!-- Pregunta  13-->
	<div class="form-group">
		<label> <b>13. Vosotros ________ cada día en el mercado.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="13.1" value="Correcto" v-model="a1p13r" :disabled="disabled == 1 ? true : false">
			<label class="" for="13.1" valu>compráis</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="13.2" value="Incorrecto2" v-model="a1p13r" :disabled="disabled == 1 ? true : false">
			<label class="" for="13.2">compramos </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="13.3" value="Incorrecto" v-model="a1p13r" :disabled="disabled == 1 ? true : false">
			<label class="" for="13.3">compran</label>
		</div>
		<div v-bind:class="comprobar13"  v-if="resultadosa1"> <h4>Pregunta número 13: {{resultado13}}</h4> </div>	
	</div>
	<!-- Pregunta  14-->
	<div class="form-group">
		<label> <b>14. ________ tomo café para desayunar.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="14.1" value="Incorrecto" v-model="a1p14r" :disabled="disabled == 1 ? true : false">
			<label class="" for="14.1" valu>Por las tardes </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="14.2" value="Incorrecto2" v-model="a1p14r" :disabled="disabled == 1 ? true : false">
			<label class="" for="14.2">Por las noches</label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="14.3" value="Correcto" v-model="a1p14r" :disabled="disabled == 1 ? true : false">
			<label class="" for="14.3">Por las mañanas</label>
		</div>
		<div v-bind:class="comprobar14"  v-if="resultadosa1"> <h4>Pregunta número 14: {{resultado14}}</h4> </div>
	</div>
	<!-- Pregunta  15-->
	<div class="form-group">
		<label> <b>15. En esta casa no ______ calefacción.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="15.1" value="Incorrecto" v-model="a1p15r" :disabled="disabled == 1 ? true : false">
			<label class="" for="15.1" valu>está  </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="15.2" value="Incorrecto2" v-model="a1p15r" :disabled="disabled == 1 ? true : false">
			<label class="" for="15.2">tiene </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="15.3" value="Correcto" v-model="a1p15r" :disabled="disabled == 1 ? true : false">
			<label class="" for="15.3">hay </label>
		</div>
		<div v-bind:class="comprobar15"  v-if="resultadosa1"> <h4>Pregunta número 15: {{resultado15}}</h4> </div>
	</div>
	<!-- Pregunta  16-->
	<div class="form-group">
		<label> <b>16. ¿Tienes ______ para beber?</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="16.1" value="Incorrecto" v-model="a1p16r" :disabled="disabled == 1 ? true : false">
			<label class="" for="16.1" valu>alguien </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="16.2" value="Correcto" v-model="a1p16r" :disabled="disabled == 1 ? true : false">
			<label class="" for="16.2">algo </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="16.3" value="Incorrecto2" v-model="a1p16r" :disabled="disabled == 1 ? true : false">
			<label class="" for="16.3">alguno </label>
		</div>
		<div v-bind:class="comprobar16"  v-if="resultadosa1"> <h4>Pregunta número 16: {{resultado16}}</h4> </div>
	</div>
	<!-- Pregunta  17-->
	<div class="form-group">
		<label> <b>17. Me ______ muy bien tus amigos.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="17.1" value="Incorrecto" v-model="a1p17r" :disabled="disabled == 1 ? true : false">
			<label class="" for="17.1" valu>llevo </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="17.2" value="Correcto" v-model="a1p17r" :disabled="disabled == 1 ? true : false">
			<label class="" for="17.2">caen </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="17.3" value="Incorrecto2" v-model="a1p17r" :disabled="disabled == 1 ? true : false">
			<label class="" for="17.3">gustan </label>
		</div>
		<div v-bind:class="comprobar17"  v-if="resultadosa1"> <h4>Pregunta número 17: {{resultado17}}</h4> </div>
	</div>
	<!-- Pregunta  18-->
	<div class="form-group">
		<label> <b>18. Me ________ mucho tus zapatos.</b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="18.1" value="Correcto" v-model="a1p18r" :disabled="disabled == 1 ? true : false">
			<label class="" for="18.1" valu>gustan </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="18.2" value="Incorrecto" v-model="a1p18r" :disabled="disabled == 1 ? true : false">
			<label class="" for="18.2">gusta  </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="18.3" value="Incorrecto2" v-model="a1p18r" :disabled="disabled == 1 ? true : false">
			<label class="" for="18.3">gustas </label>
		</div>
		<div v-bind:class="comprobar18"  v-if="resultadosa1"> <h4>Pregunta número 18: {{resultado18}}</h4> </div>
	</div>
	<!-- Pregunta  19-->
	<div class="form-group">
		<label> <b>19. No he ______ a Martina esta mañana. </b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="19.1" value="Incorrecto" v-model="a1p19r" :disabled="disabled == 1 ? true : false">
			<label class="" for="19.1" valu>veído </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="19.2" value="Correcto" v-model="a1p19r" :disabled="disabled == 1 ? true : false">
			<label class="" for="19.2">visto </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="19.3" value="Incorrecto2" v-model="a1p19r" :disabled="disabled == 1 ? true : false">
			<label class="" for="19.2">viendo </label>
		</div>
		<div v-bind:class="comprobar19"  v-if="resultadosa1"> <h4>Pregunta número 19: {{resultado19}}</h4> </div>
	</div>
	<!-- Pregunta  20-->
	<div class="form-group">
		<label> <b>20. Ya no compro cigarrillos, he _____ fumar. </b></label><br>	
		<div class="form-group resp">
			<input class="" type="radio"  id="20.1" value="Correcto" v-model="a1p20r" :disabled="disabled == 1 ? true : false">
			<label class="" for="one" valu>dejado de </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="20.2" value="Incorrecto" v-model="a1p20r" :disabled="disabled == 1 ? true : false">
			<label class="" for="two">acabado a  </label>
		</div>
		<div class="form-group resp">
			<input class="" type="radio"  id="20.3" value="Incorrecto2" v-model="a1p20r" :disabled="disabled == 1 ? true : false">
			<label class="" for="three">terminado </label>
		</div>
		<div v-bind:class="comprobar20"  v-if="resultadosa1"> <h4>Pregunta número 20: {{resultado20}}</h4> </div>
	</div>
		<button class="btn btn-primary" v-if="botoncorregir" v-on:click="validar" onclick="parar();"> Corregir	</button>
</div>
<div v-if="resultadosa1">
	<form action="registro.php" method="POST">
			<div class="container-fluid">
				<p>Completa los siguientes campos:</p>
				<div class="row">
					<div style="border: 1px solid #eee; padding: 20px; background: #eee;" class="col-md-6">
						<div class="form-group">
						<label for="nombre" style="color: #0080FF;">Nombres  <span><em></em></span></label>
						<input type="text" name="nombres" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="apellido" style="color: #0080FF;">Apellidos <span><em></em></span></label>
							<input type="text" name="apellidos" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="email" style="color: #0080FF;">Email<span><em></em></span></label>
							<input type="email" name="email" class="form-control" />
						</div>
						<input type="hidden" name="puntaje" v-model="puntaje">
							<input type="hidden" id="tiempo" name="tiempo" v-model="tiempo">
						<div class="form-group">
							<input class="btn btn-primary" name="submit" type="submit" value="Enviar" />
						</div>
						
					</div>
					<div style="border: 1px solid #000; padding: 20px; background: #eee;" class="col-md-6 ">
							<center><h2 style="margin-top: 20px;">Resultados</h2></center>
							<hr>
							<h4>Tu puntuación es: {{puntaje}}</h4>
							<h4>Preguntas correctas: {{correctas}}</h4>
							<h4>Preguntas incorrectas: {{incorrectas}}</h4>
							<h4>Tiempo transcurrido: {{tiempo}}</h4>
					</div>
				</div>
			</div>
	</form>


</div>
<div v-if="testa2">
    <h1>Test Nivel A2 Básico.</h1>
    <!-- Pregunta 1 -->
    <div class="form-group">
		<label class="testpregunta"> <b>1. Queremos ______ a Londres.</b></label>	
		<div class="form-group resp" >
			<input  type="radio" id="a1.1" value="Correcto" v-model="a2p1r" :disabled="disabled == 1 ? true : false">
        	<label for="a1.1" >Viajar</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a1.2" value="Incorrecto2" v-model="a2p1r" :disabled="disabled == 1 ? true : false">
	        <label for="a1.2">Viajaremos</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a1.3" value="Incorrecto" v-model="a2p1r" :disabled="disabled == 1 ? true : false">
	        <label for="a1.3">Viajado</label>
		</div>
		<div v-bind:class="comprobar1" v-if="resultadosa1" > <h4 > Pregunta número 1: {{resultado1}}</h4> </div>
	</div>   
    <!-- Pregunta 2 -->
    <div class="form-group">
		<label class="testpregunta"> <b>2. Voy a casa _______ pie.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a2.1" value="Correcto" v-model="a2p2r" :disabled="disabled == 1 ? true : false">
       		 <label for="a2.1" >a</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a2.2" value="Incorrecto2" v-model="a2p2r" :disabled="disabled == 1 ? true : false">
	        <label for="a2.2">en</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a2.3" value="Incorrecto" v-model="a2p2r" :disabled="disabled == 1 ? true : false">
      	    <label for="a2.3">por</label>
			
		</div>
		<div v-bind:class="comprobara2"  v-if="resultadosa2"> <h4>Pregunta número 2: {{resultadoa2}}</h4> </div>
	</div> 
    <!-- Pregunta 3 -->
    <div class="form-group">
		<label class="testpregunta"> <b>3. ¿Dónde ________ ahora?</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a3.1" value="Incorrecto" v-model="a2p3r" :disabled="disabled == 1 ? true : false">
        	<label for="a3.1" >Estuviste </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a3.2" value="Correcto" v-model="a2p3r" :disabled="disabled == 1 ? true : false">
       	 	<label for="a3.2">Estás</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a3.3" value="Incorrecto2" v-model="a2p3r" :disabled="disabled == 1 ? true : false">
      	    <label for="a3.3">Has estado</label>
		</div>
		<div v-bind:class="comprobara3"  v-if="resultadosa2"> <h4>Pregunta número 3: {{resultadoa3}}</h4> </div>
	</div> 
    <!-- Pregunta 4 -->
     <div class="form-group">
		<label class="testpregunta"> <b>4. Ayer _________ la televisión por la tarde.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a4.1" value="Incorrecto" v-model="a2p4r" :disabled="disabled == 1 ? true : false">
       		<label for="a4.1">he visto </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a4.2" value="Incorrecto2" v-model="a2p4r" :disabled="disabled == 1 ? true : false">
	        <label for="a4.2">había visto</label>
		</div>
		<div class="form-group resp" >	
		    <input type="radio" id="a4.3" value="Correcto" v-model="a2p4r" :disabled="disabled == 1 ? true : false">
       	    <label for="a4.3">vi</label>
		</div>
		<div v-bind:class="comprobara4"  v-if="resultadosa2"> <h4>Pregunta número 4: {{resultadoa4}}</h4> </div>
	</div>     
    <!-- Pregunta 5 -->
    <div class="form-group">
		<label class="testpregunta"> <b>5. Cuando viajé a Roma el año _______, visité muchos museos.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a5.1" value="Correcto" v-model="a2p5r" :disabled="disabled == 1 ? true : false">
       		<label for="a5.1" valu>pasado </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a5.2" value="Incorrecto" v-model="a2p5r" :disabled="disabled == 1 ? true : false">
       		<label for="a5.2">siguente</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a5.3" value="Incorrecto2" v-model="a2p5r" :disabled="disabled == 1 ? true : false">
       		<label for="a5.3">próximo </label>
		</div>
		<div v-bind:class="comprobara5"  v-if="resultadosa2"> <h4>Pregunta número 5: {{resultadoa5}}</h4> </div>
	</div> 
	<!-- Pregunta 6 -->
	<div class="form-group">
		<label class="testpregunta"> <b>6. Todavía vive en España porque sigue ______  en Barcelona.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a6.1" value="Incorrecto" v-model="a2p6r" :disabled="disabled == 1 ? true : false">
        	<label for="a6.1" valu>trabajar  </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a6.2" value="Correcto" v-model="a2p6r" :disabled="disabled == 1 ? true : false">
       		<label for="a6.2">trabajando</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a6.3" value="Incorrecto2" v-model="a2p6r" :disabled="disabled == 1 ? true : false">
        	<label for="a6.3">trabajado</label>
		</div>
		<div v-bind:class="comprobara6"  v-if="resultadosa2"> <h4>Pregunta número 6: {{resultadoa6}}</h4> </div>
	</div>      
    <!-- Pregunta 7 -->
    <div class="form-group">
		<label class="testpregunta"> <b>7. No estaba, después de comer le _______ a llamar.</b></label>	
		<div class="form-group resp" >
			 <input type="radio" id="a7.1" value="Incorrecto" v-model="a2p7r" :disabled="disabled == 1 ? true : false">
       		 <label for="a7.1" valu>seguiré</label>
		</div>
		<div class="form-group resp" >
			 <input type="radio" id="a7.2" value="Correcto" v-model="a2p7r" :disabled="disabled == 1 ? true : false">
       		 <label for="a7.2">volveré</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a7.3" value="Incorrecto2" v-model="a2p7r" :disabled="disabled == 1 ? true : false">
       		<label for="a7.3">comenzaré</label>
		</div>
		<div v-bind:class="comprobara7"  v-if="resultadosa2"> <h4>Pregunta número 7: {{resultadoa7}}</h4> </div>
	</div>      
    <!-- Pregunta 8 -->
     <div class="form-group">
		<label class="testpregunta"> <b>8. _______ tenía sueño, me eché la siesta. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a8.1" value="Incorrecto2" v-model="a2p8r" :disabled="disabled == 1 ? true : false">
        	<label for="a8.1" valu>Por que </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a8.2" value="Correcto" v-model="a2p8r" :disabled="disabled == 1 ? true : false">
        	<label for="a8.2">Como</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a8.3" value="Incorrecto" v-model="a2p8r" :disabled="disabled == 1 ? true : false">
       		<label for="a8.3">Así que</label>
		</div>
		<div v-bind:class="comprobara8"  v-if="resultadosa2"> <h4>Pregunta número 8: {{resultadoa8}}</h4> </div>
	</div> 
    <!-- Pregunta  9-->
    <div class="form-group">
		<label class="testpregunta"> <b>9. ¿Has comprado la comida para el perro? Si, _____ he puesto en el armario. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a9.1" value="Incorrecto2" v-model="a2p9r" :disabled="disabled == 1 ? true : false">
        	<label for="a9.1" valu>lo</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a9.2" value="Incorrecto" v-model="a2p9r" :disabled="disabled == 1 ? true : false">
       		<label for="a9.2">le </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a9.3" value="Correcto" v-model="a2p9r" :disabled="disabled == 1 ? true : false">
       		<label for="a9.3">la </label>
		</div>
		<div v-bind:class="comprobara9"  v-if="resultadosa2"> <h4>Pregunta número 9: {{resultadoa9}}</h4> </div>
	</div>
    <!-- Pregunta  10-->
    <div class="form-group">
		<label class="testpregunta"> <b>10. Tenemos un regalo para vosotros, ______ vamos a llevar esta tarde. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a20.1" value="Incorrecto2" v-model="a2p10r" :disabled="disabled == 1 ? true : false">
       		<label for="a20.1" valu>le</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a20.2" value="Incorrecto" v-model="a2p10r" :disabled="disabled == 1 ? true : false">
        	<label for="a20.2">los</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a20.3" value="Correcto" v-model="a2p10r" :disabled="disabled == 1 ? true : false">
        	<label for="a20.3">lo </label>
		</div>
		<div v-bind:class="comprobara10"  v-if="resultadosa2"> <h4>Pregunta número 10: {{resultadoa10}}</h4> </div>
	</div>    
    <!-- Pregunta  11-->
    <div class="form-group">
		<label class="testpregunta"> <b>11. Entonces _______ a las diez en la Plaza Trinidad.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a21.1" value="Incorrecto" v-model="a2p11r" :disabled="disabled == 1 ? true : false">
        	<label for="a21.1" valu>nos quedamos</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a21.2" value="Correcto" v-model="a2p11r" :disabled="disabled == 1 ? true : false">
        	<label for="a21.2">quedamos</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a21.3" value="Incorrecto2" v-model="a2p11r" :disabled="disabled == 1 ? true : false">
        	<label for="a21.3">citamos</label>
		</div>
		<div v-bind:class="comprobara11"  v-if="resultadosa2"> <h4>Pregunta número 11: {{resultadoa11}}</h4> </div>
	</div>         
    <!-- Pregunta  12-->
    <div class="form-group">
		<label class="testpregunta"> <b>12. Señor Cristobal, ¿ ___ va bien a las tres?.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a22.1" value="Incorrecto" v-model="a2p12r" :disabled="disabled == 1 ? true : false">
        	<label for="a22.1" valu>se</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a22.2" value="Correcto" v-model="a2p12r" :disabled="disabled == 1 ? true : false">
        	<label for="a22.2">le</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a22.3" value="Incorrecto2" v-model="a2p12r" :disabled="disabled == 1 ? true : false">
        	<label for="a22.3">te</label>
		</div>
		<div v-bind:class="comprobara12"  v-if="resultadosa2"> <h4>Pregunta número 12: {{resultadoa12}}</h4> </div>
	</div>  
    <!-- Pregunta  13-->
    <div class="form-group">
		<label class="testpregunta"> <b>13. Tenía mucha prisa, ______ cogí un taxi. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a23.1" value="Incorrecto" v-model="a2p13r" :disabled="disabled == 1 ? true : false">
        	<label for="a23.1" valu>como</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a23.2" value="Incorrecto2" v-model="a2p13r" :disabled="disabled == 1 ? true : false">
        	<label for="a23.2">para </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a23.3" value="Correcto" v-model="a2p13r" :disabled="disabled == 1 ? true : false">
       	 	<label for="a23.3">así que</label>
		</div>
		<div v-bind:class="comprobara13"  v-if="resultadosa2"> <h4>Pregunta número 13: {{resultadoa13}}</h4> </div>
	</div> 
    <!-- Pregunta  14-->
    <div class="form-group">
		<label class="testpregunta"> <b>14. ¿Qué haces, vienes _______ te quedas?</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a24.1" value="Correcto" v-model="a2p14r" :disabled="disabled == 1 ? true : false">
        	<label for="a24.1" valu>o </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a24.2" value="Incorrecto2" v-model="a2p14r" :disabled="disabled == 1 ? true : false">
        	<label for="a24.2">ni</label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a24.3" value="Incorrecto" v-model="a2p14r" :disabled="disabled == 1 ? true : false">
        	<label for="a24.3">si</label>
		</div>
		<div v-bind:class="comprobara14"  v-if="resultadosa2"> <h4>Pregunta número 14: {{resultadoa14}}</h4> </div>
	</div>      
    <!-- Pregunta  15-->
    <div class="form-group">
		<label class="testpregunta"> <b>15. Deben _______, aproximadamente, las cinco. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a25.1" value="Correcto" v-model="a2p15r" :disabled="disabled == 1 ? true : false">
       		<label for="a25.1" valu>ser </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a25.2" value="Incorrecto2" v-model="a2p15r" :disabled="disabled == 1 ? true : false">
        	<label for="a25.2">estar </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a25.3" value="Incorrecto" v-model="a2p15r" :disabled="disabled == 1 ? true : false">
        	<label for="a25.3">de ser</label>
		</div>
		<div v-bind:class="comprobara15"  v-if="resultadosa2"> <h4>Pregunta número 15: {{resultadoa15}}</h4> </div>
	</div> 
    <!-- Pregunta  16-->
     <div class="form-group">
		<label class="testpregunta"> <b>16. ¿_______ vamos al cine? </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a26.1" value="Correcto" v-model="a2p16r" :disabled="disabled == 1 ? true : false">
        	<label for="a26.1" valu>Porque no </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a26.2" value="Incorrecto" v-model="a2p16r" :disabled="disabled == 1 ? true : false">
        	<label for="a26.2">Y si </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a26.3" value="Incorrecto2" v-model="a2p16r" :disabled="disabled == 1 ? true : false">
        	<label for="a26.3">A ver si  </label>
		</div>
		<div v-bind:class="comprobara16"  v-if="resultadosa2"> <h4>Pregunta número 16: {{resultadoa16}}</h4> </div>
	</div> 
    <!-- Pregunta  17-->
    <div class="form-group">
		<label class="testpregunta"> <b>17. Por favor, ______ la puerta.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a27.1" value="Incorrecto" v-model="a2p17r" :disabled="disabled == 1 ? true : false">
        	<label for="a27.1" valu>cierras </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a27.2" value="Correcto" v-model="a2p17r" :disabled="disabled == 1 ? true : false">
        	<label for="a27.2">cierra </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a27.3" value="Incorrecto2" v-model="a2p17r" :disabled="disabled == 1 ? true : false">
        	<label for="a27.3">cierres</label>
		</div>
		<div v-bind:class="comprobara17"  v-if="resultadosa2"> <h4>Pregunta número 17: {{resultadoa17}}</h4> </div>
	</div>        
    <!-- Pregunta  18-->
    <div class="form-group">
		<label class="testpregunta"> <b>18. Si ______ buen tiempo, iremos al campo.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a28.1" value="Correcto" v-model="a2p18r" :disabled="disabled == 1 ? true : false">
        	<label for="a" valu>hace </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a28.2" value="Incorrecto" v-model="a2p18r" :disabled="disabled == 1 ? true : false">
        	<label for="a">hago  </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a28.3" value="Incorrecto2" v-model="a2p18r" :disabled="disabled == 1 ? true : false">
        	<label for="a">hará </label>
		</div>
		<div v-bind:class="comprobara18"  v-if="resultadosa2"> <h4>Pregunta número 18: {{resultadoa18}}</h4> </div>
	</div>
    <!-- Pregunta  19-->
    <div class="form-group">
		<label class="testpregunta"> <b>19. Le llamé para _____ el Martes.</b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a29.1" value="Incorrecto" v-model="a2p19r" :disabled="disabled == 1 ? true : false">
        	<label for="a29.1" valu>quedo</label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a29.2" value="Incorrecto2" v-model="a2p19r" :disabled="disabled == 1 ? true : false">
       		<label for="a29.2">quedando </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a29.3" value="Correcto" v-model="a2p19r" :disabled="disabled == 1 ? true : false">
        	<label for="a29.3">quedar </label>
		</div>
		<div v-bind:class="comprobara19"  v-if="resultadosa2"> <h4>Pregunta número 19: {{resultadoa19}}</h4> </div>
	</div>
    <!-- Pregunta  20-->
     <div class="form-group">
		<label class="testpregunta"> <b>20. Si quieres aprobar el examen ______ estudiar. </b></label>	
		<div class="form-group resp" >
			<input type="radio" id="a20.1" value="Correcto" v-model="a2p20r" :disabled="disabled == 1 ? true : false">
        	<label for="a20.1" valu>tienes que  </label>
		</div>
		<div class="form-group resp" >
			<input type="radio" id="a20.2" value="Incorrecto" v-model="a2p20r" :disabled="disabled == 1 ? true : false">
        	<label for="a20.2">habría que </label>
		</div>
		<div class="form-group resp" >	
			<input type="radio" id="a20.3" value="Incorrecto2" v-model="a2p20r" :disabled="disabled == 1 ? true : false">
        	<label for="a20.3">hay que </label>
		</div>
		<div v-bind:class="comprobara20"  v-if="resultadosa2"> <h4>Pregunta número 20: {{resultadoa20}}</h4> </div>
	</div>        
        <button class="btn btn-primary" v-if="botoncorregir" v-on:click="validar2" onclick="parar();"> Corregir  </button>
 </div>
<div v-if="resultadosa2">
	<form action="registro.php" method="POST">
			<div class="container-fluid">
				<p>Completa los siguientes campos:</p>
				<div class="row">
					<div style="border: 1px solid #eee; padding: 20px; background: #eee;" class="col-md-6">
						<div class="form-group">
						<label for="nombre" style="color: #0080FF;">Nombres  <span><em></em></span></label>
						<input type="text" name="nombres" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="apellido" style="color: #0080FF;">Apellidos <span><em></em></span></label>
							<input type="text" name="apellidos" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="email" style="color: #0080FF;">Email<span><em></em></span></label>
							<input type="email" name="email" class="form-control" />
						</div>
							<input type="hidden" name="puntaje" v-model="puntaje">
							<input type="hidden" id="tiempo" name="tiempo" v-model="tiempo">
						<div class="form-group">
							<input class="btn btn-primary" name="submit" type="submit" value="Enviar" />
						</div>
					</div>
					<div style="border: 1px solid #000; padding: 20px; background: #eee;" class="col-md-6 ">
							<center><h2 style="margin-top: 20px;">Resultados</h2></center>
							<hr>
							<h4>Tu puntuación es: {{puntaje}}</h4>
						
							<h4>Preguntas correctas: {{correctas}}</h4>
							<h4>Preguntas incorrectas: {{incorrectas}}</h4>
							<h4>Tiempo transcurrido: {{tiempo}}</h4>
					</div>
				</div>
			</div>
	</form>
</div>

 <div v-if="testb1">
    <h1>Test Nivel B1 Avanzado.</h1>
   <!-- Pregunta 1 -->
<div class="form-group">
	<label class="testpregunta"> <b>1. ¿Cómo aprendías tú a escribir y a leer en Español, Sarah? </b></label>	
	<div class="form-group resp" >
		<input  type="radio" id="b1.1" value="Incorrecto2" v-model="b1p1r" :disabled="disabled == 1 ? true : false">
        <label for="b1.1" >cantar</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b1.2" value="Correcto" v-model="b1p1r" :disabled="disabled == 1 ? true : false">
        <label for="b1.2">cantado </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b1.3" value="Incorrecto" v-model="b1p1r" :disabled="disabled == 1 ? true : false">
        <label for="b1.3">cantando </label>
	</div>
	<div v-bind:class="comprobarb1" v-if="resultadosb1" > <h4 > Pregunta número 1: {{resultadob1}}</h4> </div>
</div>
 <!-- Pregunta 2 -->
<div class="form-group">
	<label class="testpregunta"> 
		<b>2. Mis maestros eran muy aburridos.
		(1) no, eran muy interesantes.
		¿Y porqué eran así (2)? <br>
		Porque nos motivaban mucho en clase. 
		</b></span><br><br> 
        <p><b>(1)</b></p>
    </label>	
	<div class="form-group resp" >
		<input type="radio" id="b2.1a" value="Correcto" v-model="b1p2ra" :disabled="disabled == 1 ? true : false">
        <label for="b2.1a" >Los míos</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b2.2a" value="Incorrecto" v-model="b1p2ra" :disabled="disabled == 1 ? true : false">
        <label for="b2.2a">Mis</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b2.3a" value="Incorrecto2" v-model="b1p2ra" :disabled="disabled == 1 ? true : false">
        <label for="b2.3a">Los de mí</label>
	</div>
	<div v-bind:class="comprobarb2a"  v-if="resultadosb1"> <h4>Pregunta número 2 A: {{resultadob2a}}</h4> </div>
	<label class="testpregunta"> 
		<p><b>(2)</b></p>
    </label>
	<div class="form-group resp">	
		<input type="radio" id="b2.1b" value="Incorrecto" v-model="b1p2rb" :disabled="disabled == 1 ? true : false">
        <label for="b2.1b" >tus </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b2.2b" value="Correcto" v-model="b1p2rb" :disabled="disabled == 1 ? true : false">
        <label for="b2.2b">los tuyos</label>
	</div><div class="form-group resp" >	
		<input type="radio" id="b2.3b" value="Incorrecto2" v-model="b1p2rb" :disabled="disabled == 1 ? true : false">
        <label for="b2.3b">los de ti</label>
	</div>
	<div v-bind:class="comprobarb2b"  v-if="resultadosb1"> <h4>Pregunta número 2 B: {{resultadob2b}}</h4> </div>
</div>
 <!-- Pregunta 3 -->
<div class="form-group">
	<label class="testpregunta"><b>3. Clases de baile Muévete 
		¿Eres un negado para bailar? ¿Se te da mal seguir el ritmo? ¿Crees que no tienes facilidad para mover tu cuerpo? Sí has respondido ‘sí’, entonces este curso es para ti. Ven y prueba una hora gratis y te sorprenderás de ti mismo. 
		Tel: 956 *** ***</b></span><br>   <br>
         <p><b>A. El curso de baile es para gente que baila bien.</b></p>
	</label>	
	<div class="form-group resp" >
		<input type="radio" id="b3.3a" value="Incorrecto" v-model="b1p3ra" :disabled="disabled == 1 ? true : false">
        <label for="b3.3a">verdadero</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b3.2a" value="Correcto" v-model="b1p3ra" :disabled="disabled == 1 ? true : false">
        <label for="b3.2a">falso</label>
	</div>
	<div v-bind:class="comprobarb3a"  v-if="resultadosb1"> <h4>Pregunta número 3, A: {{resultadob3a}}</h4> </div>
	<label class="testpregunta"><b>3.Clases de baile Muévete 
		¿Eres un negado para bailar? ¿Se te da mal seguir el ritmo? ¿Crees que no tienes facilidad para mover tu cuerpo? Sí has respondido ‘sí’, entonces este curso es para ti. Ven y prueba una hora gratis y te sorprenderás de ti mismo. 
		Tel: 956 *** ***</b></span><br>   <br>
         <p><b>A. El curso de baile es para gente que baila bien.</b></p>
	</label>
	<div class="form-group resp" >	
		<input type="radio" id="b3.1b" value="Correcto" v-model="b1p3rb" :disabled="disabled == 1 ? true : false">
        <label for="b3.1b" >verdadero </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b2.2b" value="Incorrecto" v-model="b1p3rb" :disabled="disabled == 1 ? true : false">
        <label for="b3.2b">falso </label>
	</div>
	<div v-bind:class="comprobarb3a"  v-if="resultadosb1"> <h4>Pregunta número 3, B: {{resultadob3b}}</h4> </div>
</div>
 <!-- Pregunta 4 -->
<div class="form-group">
	<label class="testpregunta"> <b>4. No aprobaré el examen con buena nota _______ no he estudiado mucho.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b4.1" value="Correcto" v-model="b1p4r" :disabled="disabled == 1 ? true : false">
        <label for="b4.1">porque </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b4.2" value="Incorrecto2" v-model="b1p4r" :disabled="disabled == 1 ? true : false">
        <label for="b4.2">para</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b4.3" value="Incorrecto" v-model="b1p4r" :disabled="disabled == 1 ? true : false">
        <label for="b4.3">pero</label>
	</div>
	<div v-bind:class="comprobarb4"  v-if="resultadosb1"> <h4>Pregunta número 4: {{resultadob4}}</h4> </div>
</div>

 <!-- Pregunta 5 -->
<div class="form-group">
	<label class="testpregunta"> <b>5. No tengo ________ libro de ella en mi casa. </b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b5.1" value="Correcto" v-model="b1p5r" :disabled="disabled == 1 ? true : false">
        <label for="one" valu>ningún </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b5.2" value="Incorrecto" v-model="b1p5r" :disabled="disabled == 1 ? true : false">
        <label for="b5.2">algún</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b5.3" value="Incorrecto2" v-model="b1p5r" :disabled="disabled == 1 ? true : false">
        <label for="b5.3">ninguno  </label>
	</div>
	<div v-bind:class="comprobarb5"  v-if="resultadosb1"> <h4>Pregunta número 5: {{resultadob5}}</h4> </div>
</div>
 <!-- Pregunta 6 -->
<div class="form-group">
	<label class="testpregunta"> <b>6. Cuando ________ de mi viaje, te llamaré.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b6.1" value="Incorrecto" v-model="b1p6r" :disabled="disabled == 1 ? true : false">
        <label for="b6.1" valu>vuelva </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b6.2" value="Correcto" v-model="b1p6r" :disabled="disabled == 1 ? true : false">
        <label for="b6.2">volveré</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b6.3" value="Incorrecto2" v-model="b1p6r" :disabled="disabled == 1 ? true : false">
        <label for="b6.3">vuelvo</label>
	</div>
	<div v-bind:class="comprobarb6"  v-if="resultadosb1"> <h4>Pregunta número 6: {{resultadob6}}</h4> </div>
</div>
 <!-- Pregunta 7 -->
<div class="form-group">
	<label class="testpregunta"> <b>7. ¿________ quién has llamado? </b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b7.1" value="Incorrecto" v-model="b1p7r" :disabled="disabled == 1 ? true : false">
        <label for="b7.1" valu>a </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b7.2" value="Correcto" v-model="b1p7r" :disabled="disabled == 1 ? true : false">
        <label for="b7.2">de</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b7.3" value="Incorrecto2" v-model="b1p7r" :disabled="disabled == 1 ? true : false">
        <label for="b7.3">por </label>
	</div>
	<div v-bind:class="comprobarb7"  v-if="resultadosb1"> <h4>Pregunta número 7: {{resultadob7}}</h4> </div>
</div>
 <!-- Pregunta 8 -->
<div class="form-group">
	<label class="testpregunta"> <b>8. Tan pronto como ________ algo de él, te avisaré.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b8.1" value="Correcto" v-model="b1p8r" :disabled="disabled == 1 ? true : false">
        <label for="b8.1" valu>sepa</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b8.2" value="Incorrecto" v-model="b1p8r" :disabled="disabled == 1 ? true : false">
        <label for="b8.2">sabré</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b8.3" value="Incorrecto2" v-model="b1p8r" :disabled="disabled == 1 ? true : false">
        <label for="b8.3">sé</label>
	</div>
	<div v-bind:class="comprobarb8"  v-if="resultadosb1"> <h4>Pregunta número 8: {{resultadob8}}</h4> </div>
</div>
 <!-- Pregunta 9 -->
<div class="form-group">
	<label class="testpregunta"> <b>9. ¿Qué es lo que más te gusta de tu marido? Me encanta que (1) la casa. También me gusta que me (2).</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b9.1a" value="Correcto" v-model="b1p9ra" :disabled="disabled == 1 ? true : false">
        <label for="b9.1a" >limpiar</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b9.2a" value="Incorrecto" v-model="b1p9ra" :disabled="disabled == 1 ? true : false">
        <label for="b9.2a">limpia</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b9.3a" value="Incorrecto2" v-model="b1p9ra" :disabled="disabled == 1 ? true : false">
        <label for="b9.3a">limpie</label>
	</div>
	<div v-bind:class="comprobarb9a"  v-if="resultadosb1"> <h4>Pregunta número 9 A: {{resultadob9a}}</h4> </div>
	<label class="testpregunta"> <b>¿Qué es lo que más te gusta de tu marido? Me encanta que (1) la casa. También me gusta que me (2).</b></label>	
	<div class="form-group resp" >	
		<input type="radio" id="b9.1b" value="Correcto" v-model="b1p9rb" :disabled="disabled == 1 ? true : false">
        <label for="b9.1b" >sorprenda </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b9.2b" value="Incorrecto" v-model="b1p9rb" :disabled="disabled == 1 ? true : false">
        <label for="b9.2b">sorprender</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b9.3b" value="Incorrecto2" v-model="b1p9rb" :disabled="disabled == 1 ? true : false">
        <label for="b9.3b">sorprende </label>
	</div>
	<div v-bind:class="comprobarb9b"  v-if="resultadosb1"> <h4>Pregunta número 9 A: {{resultadob29b}}</h4> </div>
</div>
 <!-- Pregunta 10 -->
<div class="form-group">
	<label class="testpregunta"> <b>10. ¿Qué tengo que llevar en mi maleta?
			Te recomiendo que _____  unas buenas botas. ¡Hace mucho frío! 
			¡Pero es que son tan pesadas! </b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b10.1" value="Incorrecto2" v-model="b1p10r" :disabled="disabled == 1 ? true : false">
        <label for="b10.1" valu>llevas</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b10.2" value="Correcto" v-model="b1p10r" :disabled="disabled == 1 ? true : false">
        <label for="b10.2">lleves</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b10.3" value="Incorrecto" v-model="b1p10r" :disabled="disabled == 1 ? true : false">
        <label for="b10.3">lleva </label>
	</div>
	<div v-bind:class="comprobarb10"  v-if="resultadosb1"> <h4>Pregunta número 10: {{resultadob10}}</h4> </div>
</div>
 <!-- Pregunta 11 -->
<div class="form-group">
	<label class="testpregunta"> <b>11. ¿Quieres que te _________?</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b11.1" value="Correcto" v-model="b1p11r" :disabled="disabled == 1 ? true : false">
        <label for="b11.1" valu>ayude</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b11.2" value="Incorrecto" v-model="b1p11r" :disabled="disabled == 1 ? true : false">
        <label for="b11.2">ayudo</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b11.3" value="Incorrecto2" v-model="b1p11r" :disabled="disabled == 1 ? true : false">
        <label for="b11.3">ayudé</label>
	</div>
	<div v-bind:class="comprobarb11"  v-if="resultadosb1"> <h4>Pregunta número 11: {{resultadob11}}</h4> </div>
</div>
 <!-- Pregunta 12 -->
<div class="form-group">
	<label class="testpregunta"> <b>12. Cuando llegué a la estación, el tren ya _______.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b12.1" value="Correcto" v-model="b1p12r" :disabled="disabled == 1 ? true : false">
        <label for="b12.1" valu>Se había ido</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b12.2" value="Incorrecto" v-model="b1p12r" :disabled="disabled == 1 ? true : false">
        <label for="b12.2">Se fue</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b12.3" value="Incorrecto2" v-model="b1p12r" :disabled="disabled == 1 ? true : false">
        <label for="b12.3">Se ha ido</label>
	</div>
	<div v-bind:class="comprobarb12"  v-if="resultadosb1"> <h4>Pregunta número 12: {{resultadob12}}</h4> </div>
</div>
 <!-- Pregunta 13 -->
<div class="form-group">
	<label class="testpregunta"> <b>13. Dicen que España es uno de los países más _______ del mundo, según la OMS, después de Japón.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b13.1" value="Incorrecto2" v-model="b1p13r" :disabled="disabled == 1 ? true : false">
        <label for="b13.1" valu>sonoro</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b13.2" value="Correcto" v-model="b1p13r" :disabled="disabled == 1 ? true : false">
        <label for="b13.2">ruidoso </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b13.3" value="Incorrecto" v-model="b1p13r" :disabled="disabled == 1 ? true : false">
        <label for="b13.3">sordo</label>
	</div>
	<div v-bind:class="comprobarb13"  v-if="resultadosb1"> <h4>Pregunta número 13: {{resultadob13}}</h4> </div>
</div>
 <!-- Pregunta 14 -->
<div class="form-group">
	<label class="testpregunta"> <b>14. “Proliferar” significa:</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b14.1" value="Correcto" v-model="b1p14r" :disabled="disabled == 1 ? true : false">
        <label for="b14.1" valu>multiplicarse  </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b14.2" value="Incorrecto2" v-model="b1p14r" :disabled="disabled == 1 ? true : false">
        <label for="b14.2">extenderse </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b14.3" value="Incorrecto" v-model="b1p14r" :disabled="disabled == 1 ? true : false">
        <label for="b14.3">ir más allá</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b14.4" value="Incorrecto3" v-model="b1p14r" :disabled="disabled == 1 ? true : false">
        <label for="b14.4">llenar</label>
	</div>
	<div v-bind:class="comprobarb14"  v-if="resultadosb1"> <h4>Pregunta número 14: {{resultadob14}}</h4> </div>
</div>
 <!-- Pregunta 15 -->
<div class="form-group">
	<label class="testpregunta"> <b>15. Está claro que ver mucho la televisión __________ a los niños.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b15.1" value="Incorrecto3" v-model="b1p15r" :disabled="disabled == 1 ? true : false">
        <label for="b15.1" valu>perjudique </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b15.2" value="Incorrecto2" v-model="b1p15r" :disabled="disabled == 1 ? true : false">
        <label for="b15.2">perjudicará </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b15.3" value="Correcto" v-model="b1p15r" :disabled="disabled == 1 ? true : false">
        <label for="b15.3">perjudica</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b15.4" value="Incorrecto" v-model="b1p15r" :disabled="disabled == 1 ? true : false">
        <label for="b15.4">beneficie</label>
	</div>
	<div v-bind:class="comprobarb15"  v-if="resultadosb1"> <h4>Pregunta número 15: {{resultadob15}}</h4> </div>
</div>
 <!-- Pregunta 16 -->
<div class="form-group">
	<label class="testpregunta"> <b>16. Juan y su hermana Ángeles _______ tres años.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b16.1" value="Correcto" v-model="b1p16r" :disabled="disabled == 1 ? true : false">
        <label for="b16.1" valu>tienen más de</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b16.2" value="Incorrecto" v-model="b1p16r" :disabled="disabled == 1 ? true : false">
        <label for="b16.2">son menos de </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b16.3" value="Correcto" v-model="b1p16r" :disabled="disabled == 1 ? true : false">
        <label for="b16.3">se llevan </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b16.4" value="Incorrecto3" v-model="b1p16r" :disabled="disabled == 1 ? true : false">
        <label for="b16.4">es mayor de </label>
	</div>
	<div v-bind:class="comprobarb16"  v-if="resultadosb1"> <h4>Pregunta número 16: {{resultadob16}}</h4> </div>
</div>
 <!-- Pregunta 17 -->
<div class="form-group">
	<label class="testpregunta"> <b>17. ¿Y Juan? ¿Qué sabes de él?
       Nada, se fue de la empresa sin que nadie ___________</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b17.1" value="Incorrecto" v-model="b1p17r" :disabled="disabled == 1 ? true : false">
        <label for="b17.1" valu>dijera algo </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b17.2" value="Incorrecto2" v-model="b1p17r" :disabled="disabled == 1 ? true : false">
        <label for="b17.2">vio nada </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b17.3" value="Correcto" v-model="b1p17r" :disabled="disabled == 1 ? true : false">
        <label for="b17.3">se enterara </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b17.4" value="Incorrecto3" v-model="b1p17r" :disabled="disabled == 1 ? true : false">
        <label for="b17.4">estaba allí </label>
	</div>
	<div v-bind:class="comprobarb17"  v-if="resultadosb1"> <h4>Pregunta número 17: {{resultadob17}}</h4> </div>
</div>

 <!-- Pregunta 18 -->
<div class="form-group">
	<label class="testpregunta"> <b>18. Si vienes, te _______.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b18.1" value="Incorrecto" v-model="b1p18r" :disabled="disabled == 1 ? true : false">
        <label for="b18.1" valu>diviertas </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b18.2" value="Correcto" v-model="b1p18r" :disabled="disabled == 1 ? true : false">
        <label for="b18.2">divertirás  </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b18.3" value="Incorrecto2" v-model="b1p18r" :disabled="disabled == 1 ? true : false">
        <label for="b18.3">diviértete </label>
	</div>
	<div v-bind:class="comprobarb18"  v-if="resultadosb1"> <h4>Pregunta número 18: {{resultadob18}}</h4> </div>
</div>
 <!-- Pregunta 19 -->
<div class="form-group">
	<label class="testpregunta"> <b>19. Siempre te digo que ________ a mi casa, pero nunca vienes.</b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b19.1" value="Correcto" v-model="b1p19r" :disabled="disabled == 1 ? true : false">
        <label for="b19.1" valu>Vengas</label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b19.2" value="Incorrecto2" v-model="b1p19r" :disabled="disabled == 1 ? true : false">
        <label for="b19.2">Ven</label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b19.3" value="Incorrecto" v-model="b1p19r" :disabled="disabled == 1 ? true : false">
        <label for="b19.3">Vienes </label>
	</div>
	<div v-bind:class="comprobarb19"  v-if="resultadosb1"> <h4>Pregunta número 19: {{resultadob19}}</h4> </div>
</div>
 <!-- Pregunta 20 -->
<div class="form-group">
	<label class="testpregunta"> <b>20. Cuando estudiaba en la universidad, ________ seis horas diarias. </b></label>	
	<div class="form-group resp" >
		<input type="radio" id="b20.1" value="Correcto" v-model="b1p20r" :disabled="disabled == 1 ? true : false">
        <label for="b20.1" valu>Dormiré  </label>
	</div>
	<div class="form-group resp" >
		<input type="radio" id="b20.2" value="Incorrecto" v-model="b1p20r" :disabled="disabled == 1 ? true : false">
        <label for="b20.2">Dormía </label>
	</div>
	<div class="form-group resp" >	
		<input type="radio" id="b20.3" value="Incorrecto2" v-model="b1p20r" :disabled="disabled == 1 ? true : false">
        <label for="b20.3">He dormido. </label>
	</div>
	<div v-bind:class="comprobarb20"  v-if="resultadosb1"> <h4>Pregunta número 20: {{resultadob20}}</h4> </div>
</div>
        
        <button class="btn btn-primary" v-if="botoncorregir" v-on:click="validar3" onclick="parar();"> Corregir  </button>
 </div>
<div v-if="resultadosb1">
	<form action="registro.php" method="POST">
			<div class="container-fluid">
				<p>Completa los siguientes campos:</p>
				<div class="row">
					<div style="border: 1px solid #eee; padding: 20px; background: #eee;" class="col-md-6">
						<div class="form-group">
						<label for="nombre" style="color: #0080FF;">Nombres  <span><em></em></span></label>
						<input type="text" name="nombres" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="apellido" style="color: #0080FF;">Apellidos <span><em></em></span></label>
							<input type="text" name="apellidos" class="form-control" required/>
						</div>   
						<div class="form-group">
							<label for="email" style="color: #0080FF;">Email<span><em></em></span></label>
							<input type="email" name="email" class="form-control" />
						</div>
						<input type="hidden" name="puntaje" v-model="puntaje">
							<input type="hidden" id="tiempo" name="tiempo" v-model="tiempo">
						<div class="form-group">
							<input class="btn btn-primary" name="submit" type="submit" value="Enviar" />
						</div>
					</div>
					<div style="border: 1px solid #000; padding: 20px; background: #eee;" class="col-md-6 ">
							<center><h2 style="margin-top: 20px;">Resultados</h2></center>
							<hr>
							<h4>Tu puntuación es: {{puntaje}}</h4>
							
							<h4>Preguntas correctas: {{correctas}}</h4>
							<h4>Preguntas incorrectas: {{incorrectas}}</h4>
							<h4>Tiempo transcurrido: {{tiempo}}</h4>
					</div>
				</div>
			</div>
	</form>
</div>

</div>
	
 
	</div>
	    <script src="js/vue.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script>
		var centesimas = 0;
		var segundos = 0;
		var minutos = 0;
		var horas = 0;
		function inicio () {
			control = setInterval(cronometro,10);
			document.getElementById("inicio").disabled = true;
			document.getElementById("parar").disabled = false;
			document.getElementById("continuar").disabled = true;
			document.getElementById("reinicio").disabled = false;
		}
		function parar () {
			clearInterval(control);
			var tiempo = minutos+':'+segundos;
			document.getElementById("parar").disabled = true;
			document.getElementById("continuar").disabled = false;
			app.tiempo = tiempo;
			
		}
		function reinicio () {
			clearInterval(control);
			centesimas = 0;
			segundos = 0;
			minutos = 0;
			horas = 0;
			Centesimas.innerHTML = ":00";
			Segundos.innerHTML = ":00";
			Minutos.innerHTML = ":00";
			Horas.innerHTML = "00";
			document.getElementById("inicio").disabled = false;
			document.getElementById("parar").disabled = true;
			document.getElementById("continuar").disabled = true;
			document.getElementById("reinicio").disabled = true;
		}
		function cronometro () {
			if (centesimas < 99) {
				centesimas++;
				if (centesimas < 10) { centesimas = "0"+centesimas }
				Centesimas.innerHTML = ":"+centesimas;
			}
			if (centesimas == 99) {
				centesimas = -1;
			}
			if (centesimas == 0) {
				segundos ++;
				if (segundos < 10) { segundos = "0"+segundos }
				Segundos.innerHTML = ":"+segundos;
			}
			if (segundos == 59) {
				segundos = -1;
			}
			if ( (centesimas == 0)&&(segundos == 0) ) {
				minutos++;
				if (minutos < 10) { minutos = "0"+minutos }
				Minutos.innerHTML = ":"+minutos;
			}
			if (minutos == 59) {
				minutos = -1;
			}
			if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
				horas ++;
				if (horas < 10) { horas = "0"+horas }
				Horas.innerHTML = horas;
			}
		}
	</script>

    
    
</body>
</html>



