


var app = new Vue({
  el: '#app',
  data: {
    tiempo: '',
    /*  TEST A1   */
    a1p1: 'Amarillos',
    a1p1r: 0,
    a1p2: 'por',
    a1p2r: 0,
    a1p3: 'Cuántos',
    a1p3r: 0,
    a1p4: 'que',
    a1p4r: 0,
    a1p5: 'cuarenta y ocho',
    a1p5r: 0,
    a1p6: 'tengo',
    a1p6r: 0,
    a1p7: 'me',
    a1p7r: 0,
    a1p8: 'es/está',
    a1p8r: 0,
    a1p9: 'Estudió',
    a1p9r: 0,
    a1p10: 'eres',
    a1p10r: 0,
    a1p11: 'nos',
    a1p11r: 0,
    a1p12: 'las',
    a1p12r: 0,
    a1p13: 'compráis',
    a1p13r: 0,
    a1p14: 'Por las mañanas',
    a1p14r: 0,
    a1p15: 'hay',
    a1p15r: 0,
    a1p16: 'algo',
    a1p16r: 0,
    a1p17: 'caen',
    a1p17r: 0,
    a1p18: 'gustan',
    a1p18r: 0,
    a1p19: 'visto',
    a1p19r: 0,
    a1p20: 'dejado de',
    a1p20r: 0,
    resultado1: 'vacio',
    resultado2: 'vacio',
    resultado3: 'vacio',
    resultado4: 'vacio',
    resultado5: 'vacio',
    resultado6: 'vacio',
    resultado7: 'vacio',
    resultado8: 'vacio',
    resultado9: 'vacio',
    resultado10: 'vacio',
    resultado11: 'vacio',
    resultado12: 'vacio',
    resultado13: 'vacio',
    resultado14: 'vacio',
    resultado15: 'vacio',
    resultado16: 'vacio',
    resultado17: 'vacio',
    resultado18: 'vacio',
    resultado18: 'vacio',
    resultado20: 'vacio',
    comprobar1: 'vacio',
    comprobar2: 'vacio',
    comprobar3: 'vacio',
    comprobar4: 'vacio',
    comprobar5: 'vacio',
    comprobar6: 'vacio',
    comprobar7: 'vacio',
    comprobar8: 'vacio',
    comprobar9: 'vacio',
    comprobar10: 'vacio',
    comprobar11: 'vacio',
    comprobar12: 'vacio',
    comprobar13: 'vacio',
    comprobar14: 'vacio',
    comprobar15: 'vacio',
    comprobar16: 'vacio',
    comprobar17: 'vacio',
    comprobar18: 'vacio',
    comprobar19: 'vacio',
    comprobar20: 'vacio',
    /* TEST A2*/
      a2p1: 'Viajar',
    a2p1r: 0,
    a2p2: 'a',
    a2p2r: 0,
    a2p3: 'Estás',
    a2p3r: 0,
    a2p4: 'vi',
    a2p4r: 0,
    a2p5: 'pasado',
    a2p5r: 0,
    a2p6: 'trabajando',
    a2p6r: 0,
    a2p7: 'volveré',
    a2p7r: 0,
    a2p8: 'Como',
    a2p8r: 0,
    a2p9: 'la',
    a2p9r: 0,
    a2p10: 'os lo',
    a2p10r: 0,
    a2p11: 'quedamos',
    a2p11r: 0,
    a2p12: 'le',
    a2p12r: 0,
    a2p13: 'así que',
    a2p13r: 0,
    a2p14: 'o',
    a2p14r: 0,
    a2p15: 'ser',
    a2p15r: 0,
    a2p16: 'Porque no',
    a2p16r: 0,
    a2p17: 'cierra',
    a2p17r: 0,
    a2p18: 'hace',
    a2p18r: 0,
    a2p19: 'quedar',
    a2p19r: 0,
    a2p20: 'tienes que',
    a2p20r: 0,
    resultadoa1: 'vacio',
    resultadoa2: 'vacio',
    resultadoa3: 'vacio',
    resultadoa4: 'vacio',
    resultadoa5: 'vacio',
    resultadoa6: 'vacio',
    resultadoa7: 'vacio',
    resultadoa8: 'vacio',
    resultadoa9: 'vacio',
    resultadoa10: 'vacio',
    resultadoa11: 'vacio',
    resultadoa12: 'vacio',
    resultadoa13: 'vacio',
    resultadoa14: 'vacio',
    resultadoa15: 'vacio',
    resultadoa16: 'vacio',
    resultadoa17: 'vacio',
    resultadoa18: 'vacio',
    resultadoa18: 'vacio',
    resultadoa20: 'vacio',
    comprobara1: 'vacio',
    comprobara2: 'vacio',
    comprobara3: 'vacio',
    comprobara4: 'vacio',
    comprobara5: 'vacio',
    comprobara6: 'vacio',
    comprobara7: 'vacio',
    comprobara8: 'vacio',
    comprobara9: 'vacio',
    comprobara10: 'vacio',
    comprobara11: 'vacio',
    comprobara12: 'vacio',
    comprobara13: 'vacio',
    comprobara14: 'vacio',
    comprobara15: 'vacio',
    comprobara16: 'vacio',
    comprobara17: 'vacio',
    comprobara18: 'vacio',
    comprobara19: 'vacio',
    comprobara20: 'vacio',
    /* TEST B1*/
    b1p1: 'cantado',
    b1p1r: 0,
    b1p2a: 'Los míos',
    b1p2ra: 0,
    b1p2b: 'los tuyos',
    b1p2rb: 0,
    b1p3a: 'falso',
    b1p3ra: 0,
    b1p3b: 'verdadero',
    b1p3rb: 0,
    b1p4: 'porque',
    b1p4r: 0,
    b1p5: 'ningún',
    b1p5r: 0,
    b1p6: 'vuelva',
    b1p6r: 0,
    b1p7: 'a',
    b1p7r: 0,
    b1p8: 'sepa',
    b1p8r: 0,
    b1p9a: 'limpie',
    b1p9ra: 0,
    b1p9b: 'sorprenda',
    b1p9rb: 0,
    b1p10: 'leves',
    b1p10r: 0,
    b1p11: 'ayude',
    b1p11r: 0,
    b1p12: 'Se había ido',
    b1p12r: 0,
    b1p13: 'ruidoso',
    b1p13r: 0,
    b1p14: 'multiplicarse',
    b1p14r: 0,
    b1p15: 'perjudica',
    b1p15r: 0,
    b1p16: 'se llevan',
    b1p16r: 0,
    b1p17: 'se enterara',
    b1p17r: 0,
    b1p18: 'divertirás',
    b1p18r: 0,
    b1p19: 'Vengas',
    b1p19r: 0,
    b1p20: 'Dormía',
    b1p20r: 0,
    resultadob1: 'vacio',
    resultadob2a: 'vacio',
    resultadob2b: 'vacio',
    resultadob3a: 'vacio',
    resultadob3b: 'vacio',
    resultadob4: 'vacio',
    resultadob5: 'vacio',
    resultadob6: 'vacio',
    resultadob7: 'vacio',
    resultadob8: 'vacio',
    resultadob9a: 'vacio',
    resultadob9b: 'vacio',
    resultadob10: 'vacio',
    resultadob11: 'vacio',
    resultadob12: 'vacio',
    resultadob13: 'vacio',
    resultadob14: 'vacio',
    resultadob15: 'vacio',
    resultadob16: 'vacio',
    resultadob17: 'vacio',
    resultadob18: 'vacio',
    resultadob18: 'vacio',
    resultadob20: 'vacio',
    comprobarb1: 'vacio',
    comprobarb2a: 'vacio',
    comprobarb2b: 'vacio',
    comprobarb3a: 'vacio',
    comprobarb3b: 'vacio',
    comprobarb4: 'vacio',
    comprobarb5: 'vacio',
    comprobarb6: 'vacio',
    comprobarb7: 'vacio',
    comprobarb8: 'vacio',
    comprobarb9a: 'vacio',
    comprobarb9b: 'vacio',
    comprobarb10: 'vacio',
    comprobarb11: 'vacio',
    comprobarb12: 'vacio',
    comprobarb13: 'vacio',
    comprobarb14: 'vacio',
    comprobarb15: 'vacio',
    comprobarb16: 'vacio',
    comprobarb17: 'vacio',
    comprobarb18: 'vacio',
    comprobarb19: 'vacio',
    comprobarb20: 'vacio',
    puntaje: 0,
    resultadosa1:false,
    resultadosa2:false,
    resultadosb1:false,
    correctas: 0,
    incorrectas: 0,
    disabled: 0,
    menuinicial: true,
    testa1: false,
    testa2: false,
    testb1: false,
    botoncorregir: true,
  }, 
  methods: {
    abrirtesta1: function()
    {
        this.menuinicial = false;
        this.testa2 = false;
        this.testb1 = false;
        this.testa1 = true;
    },
    abrirtesta2: function()
    {
        this.menuinicial = false;
        this.testa2 = true;
        this.testb1 = false;
        this.testa1 = false;
    },
    abrirtestb1: function()
    {
        this.menuinicial = false;
        this.testa2 = false;
        this.testb1 = true;
        this.testa1 = false;
    },
  	validar: function()
  	{
        this.botoncorregir = false;
  		this.resultadosa1 = true;
      this.disabled = (this.disabled + 1) % 2;

        if(this.a1p1r== 'Correcto'){
           this.resultado1 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
           this.correctas= this.correctas + 1;
           this.comprobar1= 'buena';
        }
        else{
         this.resultado1 = 'Incorrecto. La respuesta correcta es:'+this.a1p1;
         this.incorrectas= this.incorrectas + 1
         this.comprobar1= 'mala';
        }
        if(this.a1p2r== 'Correcto'){
           this.resultado2 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar2= 'buena';
        }
        else{
         this.resultado2 = 'Incorrecto. La respuesta correcta es: '+this.a1p2;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar2= 'mala';
        }
        if(this.a1p3r== 'Correcto'){
           this.resultado3 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar3= 'buena';
        }
        else{
         this.resultado3 = 'Incorrecto. La respuesta correcta es: '+this.a1p3;
         this.incorrectas= this.incorrectas + 1
         this.comprobar3= 'mala';
        }
        if(this.a1p4r== 'Correcto'){
           this.resultado4 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar4= 'buena';
        }
        else{
         this.resultado4 = 'Incorrecto. La respuesta correcta es: '+this.a1p4;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar4= 'mala';
        }
        if(this.a1p5r== 'Correcto'){
           this.resultado5 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar5= 'buena';
        }
        else{
         this.resultado5 = 'Incorrecto. La respuesta correcta es: '+this.a1p5;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar5= 'mala';
        }
        if(this.a1p6r== 'Correcto'){
           this.resultado6 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar6= 'buena';
        }
        else{
         this.resultado6 = 'Incorrecto. La respuesta correcta es: '+this.a1p6;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar6= 'mala';
        }
        if(this.a1p7r== 'Correcto'){
           this.resultado7 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar7= 'buena';
        }
        else{
         this.resultado7 = 'Incorrecto. La respuesta correcta es: '+this.a1p7;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar7= 'mala';
        }
        if(this.a1p8r== 'Correcto'){
           this.resultado8 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar8= 'buena';
        }
        else{
         this.resultado8 = 'Incorrecto. La respuesta correcta es: '+this.a1p8;
         this.incorrectas= this.incorrectas + 1
         this.comprobar8= 'mala';
        }
        if(this.a1p9r== 'Correcto'){
           this.resultado9 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar9= 'buena';
        }
        else{
         this.resultado9 = 'Incorrecto. La respuesta correcta es: '+this.a1p9;
         this.incorrectas= this.incorrectas + 1
         this.comprobar9= 'mala';
        }
        if(this.a1p10r== 'Correcto'){
           this.resultado10 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar10= 'buena';
        }
        else{
         this.resultado10 = 'Incorrecto. La respuesta correcta es: '+this.a1p10;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar10= 'mala';
        }
        if(this.a1p11r== 'Correcto'){
           this.resultado11 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar11= 'buena';
        }
        else{
         this.resultado11 = 'Incorrecto. La respuesta correcta es: '+this.a1p11;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar11= 'mala';
        }
        if(this.a1p12r== 'Correcto'){
           this.resultado12 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar12= 'buena';
        }
        else{
         this.resultado12 = 'Incorrecto. La respuesta correcta es: '+this.a1p12;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar12= 'mala';
        }
        if(this.a1p13r== 'Correcto'){
           this.resultado13 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar13= 'buena';
        }
        else{
         this.resultado13 = 'Incorrecto. La respuesta correcta es: '+this.a1p13;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar13= 'mala';
        }
        if(this.a1p14r== 'Correcto'){
           this.resultado14 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar14= 'buena';
        }
        else{
         this.resultado14 = 'Incorrecto. La respuesta correcta es: '+this.a1p14;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar14= 'mala';
        }
        if(this.a1p15r== 'Correcto'){
           this.resultado15 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar15= 'buena';
        }
        else{
         this.resultado15 = 'Incorrecto. La respuesta correcta es: '+this.a1p15;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar15= 'mala';
        }
        if(this.a1p16r== 'Correcto'){
           this.resultado16 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar16= 'buena';
        }
        else{
         this.resultado16 = 'Incorrecto. La respuesta correcta es: '+this.a1p16;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar16= 'mala';
        }
         if(this.a1p17r== 'Correcto'){
           this.resultado17 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar17= 'buena';
        }
        else{
         this.resultado17 = 'Incorrecto. La respuesta correcta es: '+this.a1p17;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar17= 'mala';
        }
         if(this.a1p18r== 'Correcto'){
           this.resultado18 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar18= 'buena';
        }
        else{
         this.resultado18 = 'Incorrecto. La respuesta correcta es: '+this.a1p18;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar18= 'mala';
        }
        if(this.a1p19r== 'Correcto'){
           this.resultado19 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar19= 'buena';
        }
        else{
         this.resultado19 = 'Incorrecto. La respuesta correcta es: '+this.a1p19;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar19= 'mala';
        }
        if(this.a1p20r== 'Correcto'){
           this.resultado20 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobar20= 'buena';
        }
        else{
         this.resultado20 = 'Incorrecto. La respuesta correcta es: '+this.a1p20;
         this.incorrectas= this.incorrectas + 1;
         this.comprobar20= 'mala';
        }        
    }, /*Cerrando Validar*/
    validar2: function()
    {
        this.botoncorregir = false;
        this.resultadosa2 = true;
      this.disabled = (this.disabled + 1) % 2;
        if(this.a2p1r== 'Correcto'){
           this.resultadoa1 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
           this.correctas= this.correctas + 1;
           this.comprobara1= 'buena';
        }
        else{
         this.resultadoa1 = 'Incorrecto. La respuesta correcta es:'+this.a2p1;
         this.incorrectas= this.incorrectas + 1
         this.comprobara1= 'mala';
        }
        if(this.a2p2r== 'Correcto'){
           this.resultadoa2 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara2= 'buena';
        }
        else{
         this.resultadoa2 = 'Incorrecto. La respuesta correcta es: '+this.a2p2;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara2= 'mala';
        }
        if(this.a2p3r== 'Correcto'){
           this.resultadoa3 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara3= 'buena';
        }
        else{
         this.resultadoa3 = 'Incorrecto. La respuesta correcta es: '+this.a2p3;
         this.incorrectas= this.incorrectas + 1
         this.comprobara3= 'mala';
        }
        if(this.a2p4r== 'Correcto'){
           this.resultadoa4 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara4= 'buena';
        }
        else{
         this.resultadoa4 = 'Incorrecto. La respuesta correcta es: '+this.a2p4;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara4= 'mala';
        }
        if(this.a2p5r== 'Correcto'){
           this.resultadoa5 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara5= 'buena';
        }
        else{
         this.resultadoa5 = 'Incorrecto. La respuesta correcta es: '+this.a2p5;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara5= 'mala';
        }
        if(this.a2p6r== 'Correcto'){
           this.resultadoa6 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara6= 'buena';
        }
        else{
         this.resultadoa6 = 'Incorrecto. La respuesta correcta es: '+this.a2p6;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara6= 'mala';
        }
        if(this.a2p7r== 'Correcto'){
           this.resultadoa7 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara7= 'buena';
        }
        else{
         this.resultadoa7 = 'Incorrecto. La respuesta correcta es: '+this.a2p7;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara7= 'mala';
        }
        if(this.a2p8r== 'Correcto'){
           this.resultadoa8 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara8= 'buena';
        }
        else{
         this.resultadoa8 = 'Incorrecto. La respuesta correcta es: '+this.a2p8;
         this.incorrectas= this.incorrectas + 1
         this.comprobara8= 'mala';
        }
        if(this.a2p9r== 'Correcto'){
           this.resultadoa9 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara9= 'buena';
        }
        else{
         this.resultadoa9 = 'Incorrecto. La respuesta correcta es: '+this.a2p9;
         this.incorrectas= this.incorrectas + 1
         this.comprobara9= 'mala';
        }
        if(this.a2p10r== 'Correcto'){
           this.resultadoa10 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara10= 'buena';
        }
        else{
         this.resultadoa10 = 'Incorrecto. La respuesta correcta es: '+this.a2p10;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara10= 'mala';
        }
        if(this.a2p11r== 'Correcto'){
           this.resultadoa11 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara11= 'buena';
        }
        else{
         this.resultadoa11 = 'Incorrecto. La respuesta correcta es: '+this.a2p11;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara11= 'mala';
        }
        if(this.a2p12r== 'Correcto'){
           this.resultadoa12 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara12= 'buena';
        }
        else{
         this.resultadoa12 = 'Incorrecto. La respuesta correcta es: '+this.a2p12;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara12= 'mala';
        }
        if(this.a2p13r== 'Correcto'){
           this.resultadoa13 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara13= 'buena';
        }
        else{
         this.resultadoa13 = 'Incorrecto. La respuesta correcta es: '+this.a2p13;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara13= 'mala';
        }
        if(this.a2p14r== 'Correcto'){
           this.resultadoa14 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara14= 'buena';
        }
        else{
         this.resultadoa14 = 'Incorrecto. La respuesta correcta es: '+this.a2p14;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara14= 'mala';
        }
        if(this.a2p15r== 'Correcto'){
           this.resultadoa15 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara15= 'buena';
        }
        else{
         this.resultadoa15 = 'Incorrecto. La respuesta correcta es: '+this.a2p15;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara15= 'mala';
        }
        if(this.a2p16r== 'Correcto'){
           this.resultadoa16 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara16= 'buena';
        }
        else{
         this.resultadoa16 = 'Incorrecto. La respuesta correcta es: '+this.a2p16;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara16= 'mala';
        }
         if(this.a2p17r== 'Correcto'){
           this.resultadoa17 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara17= 'buena';
        }
        else{
         this.resultadoa17 = 'Incorrecto. La respuesta correcta es: '+this.a2p17;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara17= 'mala';
        }
         if(this.a2p18r== 'Correcto'){
           this.resultadoa18 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara18= 'buena';
        }
        else{
         this.resultadoa18 = 'Incorrecto. La respuesta correcta es: '+this.a2p18;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara18= 'mala';
        }
        if(this.a2p19r== 'Correcto'){
           this.resultadoa19 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara19= 'buena';
        }
        else{
         this.resultadoa19 = 'Incorrecto. La respuesta correcta es: '+this.a2p19;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara19= 'mala';
        }
        if(this.a2p20r== 'Correcto'){
           this.resultadoa20 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobara20= 'buena';
        }
        else{
         this.resultadoa20 = 'Incorrecto. La respuesta correcta es: '+this.a2p20;
         this.incorrectas= this.incorrectas + 1;
         this.comprobara20= 'mala';
        }     
    }, /*Cerrando Validara2*/
    validar3: function()
    {
      this.resultadosb1 = true;
      this.disabled = (this.disabled + 1) % 2;
      this.botoncorregir = false;
        if(this.b1p1r== 'Correcto'){
           this.resultadob1 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
           this.correctas= this.correctas + 1;
           this.comprobarb1= 'buena';
        }
        else{
         this.resultadob1 = 'Incorrecto. La respuesta correcta es:'+this.b1p1;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb1= 'mala';
        }
        if(this.b1p2ra== 'Correcto'){
           this.resultadob2a = '¡Correcto!';
           this.puntaje = this.puntaje + 2.5;
            this.correctas= this.correctas + 1;
            this.comprobarb2a= 'buena';
        }
        else{
         this.resultadob2a = 'Incorrecto. La respuesta correcta es: '+this.b1p2a;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb2a= 'mala';
        }
         if(this.b1p2rb== 'Correcto'){
           this.resultadob2b = '¡Correcto!';
           this.puntaje = this.puntaje + 2.5;
            this.correctas= this.correctas + 1;
            this.comprobarb2b= 'buena';
        }
        else{
         this.resultadob2b = 'Incorrecto. La respuesta correcta es: '+this.b1p2b;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb2b= 'mala';
        }

        if(this.b1p3ra== 'Correcto'){
           this.resultadob3 = '¡Correcto!';
           this.puntaje = this.puntaje + 2.5;
            this.correctas= this.correctas + 1;
            this.comprobarb3a= 'buena';
        }
        else{
         this.resultadob3a = 'Incorrecto. La respuesta correcta es: '+this.b1p3a;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb3a= 'mala';
        }


        if(this.b1p3rb== 'Correcto'){
           this.resultadob3b = '¡Correcto!';
           this.puntaje = this.puntaje + 2.5;
            this.correctas= this.correctas + 1;
            this.comprobarb3b= 'buena';
        }
        else{
         this.resultadob3b = 'Incorrecto. La respuesta correcta es: '+this.b1p3b;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb3b= 'mala';
        }

        if(this.b1p4r== 'Correcto'){
           this.resultadob4 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb4= 'buena';
        }
        else{
         this.resultadob4 = 'Incorrecto. La respuesta correcta es: '+this.b1p4;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb4= 'mala';
        }
        if(this.b1p5r== 'Correcto'){
           this.resultadob5 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb5= 'buena';
        }
        else{
         this.resultadob5 = 'Incorrecto. La respuesta correcta es: '+this.b1p5;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb5= 'mala';
        }
        if(this.b1p6r== 'Correcto'){
           this.resultadob6 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb6= 'buena';
        }
        else{
         this.resultadob6 = 'Incorrecto. La respuesta correcta es: '+this.b1p6;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb6= 'mala';
        }
        if(this.b1p7r== 'Correcto'){
           this.resultadob7 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb7= 'buena';
        }
        else{
         this.resultadob7 = 'Incorrecto. La respuesta correcta es: '+this.b1p7;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb7= 'mala';
        }
        if(this.b1p8r== 'Correcto'){
           this.resultadob8 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb8= 'buena';
        }
        else{
         this.resultadob8 = 'Incorrecto. La respuesta correcta es: '+this.b1p8;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb8= 'mala';
        }
        if(this.b1p9ra== 'Correcto'){
           this.resultadob9 = '¡Correcto!';
           this.puntaje = this.puntaje + 2.5;
            this.correctas= this.correctas + 1;
            this.comprobarb9= 'buena';
        }
        else{
         this.resultadob9a = 'Incorrecto. La respuesta correcta es: '+this.b1p9;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb9= 'mala';
        }
         if(this.b1p9rb== 'Correcto'){
           this.resultadob9 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb9= 'buena';
        }
        else{
         this.resultadob9a = 'Incorrecto. La respuesta correcta es: '+this.b1p9b;
         this.incorrectas= this.incorrectas + 1
         this.comprobarb9= 'mala';
        }
        if(this.b1p10r== 'Correcto'){
           this.resultadob10 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb10= 'buena';
        }
        else{
         this.resultadob10 = 'Incorrecto. La respuesta correcta es: '+this.b1p10;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb10= 'mala';
        }
        if(this.b1p11r== 'Correcto'){
           this.resultadob11 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb11= 'buena';
        }
        else{
         this.resultadob11 = 'Incorrecto. La respuesta correcta es: '+this.b1p11;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb11= 'mala';
        }
        if(this.b1p12r== 'Correcto'){
           this.resultadob12 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb12= 'buena';
        }
        else{
         this.resultadob12 = 'Incorrecto. La respuesta correcta es: '+this.b1p12;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb12= 'mala';
        }
        if(this.b1p13r== 'Correcto'){
           this.resultadob13 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb13= 'buena';
        }
        else{
         this.resultadob13 = 'Incorrecto. La respuesta correcta es: '+this.b1p13;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb13= 'mala';
        }
        if(this.b1p14r== 'Correcto'){
           this.resultadob14 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb14= 'buena';
        }
        else{
         this.resultadob14 = 'Incorrecto. La respuesta correcta es: '+this.b1p14;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb14= 'mala';
        }
        if(this.b1p15r== 'Correcto'){
           this.resultadob15 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb15= 'buena';
        }
        else{
         this.resultadob15 = 'Incorrecto. La respuesta correcta es: '+this.b1p15;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb15= 'mala';
        }
        if(this.b1p16r== 'Correcto'){
           this.resultadob16 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb16= 'buena';
        }
        else{
         this.resultadob16 = 'Incorrecto. La respuesta correcta es: '+this.b1p16;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb16= 'mala';
        }
         if(this.b1p17r== 'Correcto'){
           this.resultadob17 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb17= 'buena';
        }
        else{
         this.resultadob17 = 'Incorrecto. La respuesta correcta es: '+this.b1p17;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb17= 'mala';
        }
         if(this.b1p18r== 'Correcto'){
           this.resultadob18 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb18= 'buena';
        }
        else{
         this.resultadob18 = 'Incorrecto. La respuesta correcta es: '+this.b1p18;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb18= 'mala';
        }
        if(this.b1p19r== 'Correcto'){
           this.resultadob19 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb19= 'buena';
        }
        else{
         this.resultadob19 = 'Incorrecto. La respuesta correcta es: '+this.b1p19;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb19= 'mala';
        }
        if(this.b1p20r== 'Correcto'){
           this.resultadob10 = '¡Correcto!';
           this.puntaje = this.puntaje + 5;
            this.correctas= this.correctas + 1;
            this.comprobarb20= 'buena';
        }
        else{
         this.resultadob20 = 'Incorrecto. La respuesta correcta es: '+this.b1p20;
         this.incorrectas= this.incorrectas + 1;
         this.comprobarb20= 'mala';
        }     
    } /*Cerrando Validara3*/
  } /*Cerrando metodos*/
}); /*Cerrando Vue*/